FROM ps2dev/ps2dev:latest

ENV DEBIAN_FRONTEND="noninteractive"

ARG uid
ARG branch=develop
ENV branch=$branch

RUN apk add \
    gmp \
    mpc1 \
    mpfr \
    sudo \
    make \
    git && \
    adduser -D -g '' -h /developer developer && \
    echo "developer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/developer && \
    chmod 0440 /etc/sudoers.d/developer && \
    chown -R developer:developer /developer

ENV HOME=/developer

RUN echo $'#!/bin/sh\n${PS2DEV}/ee/bin/mips64r5900el-ps2-elf-gcc "$@"' > ${PS2DEV}/ee/bin/ee-gcc && chmod +x ${PS2DEV}/ee/bin/ee-gcc && \
    echo $'#!/bin/sh\n${PS2DEV}/ee/bin/mips64r5900el-ps2-elf-g++ "$@"' > ${PS2DEV}/ee/bin/ee-g++ && chmod +x ${PS2DEV}/ee/bin/ee-g++ && \
    echo $'#!/bin/sh\n${PS2DEV}/ee/bin/mips64r5900el-ps2-elf-ar "$@"' > ${PS2DEV}/ee/bin/ee-ar && chmod +x ${PS2DEV}/ee/bin/ee-ar && \
    echo $'#!/bin/sh\n${PS2DEV}/iop/bin/mipsel-ps2-irx-gcc "$@"' > ${PS2DEV}/iop/bin/iop-gcc && chmod +x ${PS2DEV}/iop/bin/iop-gcc && \
    echo $'#!/bin/sh\n${PS2DEV}/iop/bin/mipsel-ps2-irx-g++ "$@"' > ${PS2DEV}/iop/bin/iop-g++ && chmod +x ${PS2DEV}/iop/bin/iop-g++ && \
    echo $'#!/bin/sh\n${PS2DEV}/iop/bin/mipsel-ps2-irx-ar "$@"' > ${PS2DEV}/iop/bin/iop-ar && chmod +x ${PS2DEV}/iop/bin/iop-ar

USER developer
WORKDIR /developer

CMD /bin/bash
